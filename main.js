// ************* zadanie 1 **************

function returnEvenChars(str) {
  let newString = '';
  for (i = 0; i < str.length; i++) {
      if (i % 2 === 0) {
          continue;
      }
      const currentLetter = str.charAt(i);
      newString += currentLetter;
  }
  return newString
}



// ************* zadanie 2 **************

const zwierzeta = [
  "pantera",
  "pirania",
  "łasica"
];

for (let i = 0; i < zwierzeta.length; i++) {
  console.log(`Niesamowita ${zwierzeta[i]}`)
  zwierzeta[i] = `Niesamowita ${zwierzeta[i]}`;
}


// ************* zadanie 3 **************

const alfabet = "abcdefghijklmnopqrstuvwxyz";
function pickRandomLetter() {

    const randomLetter = Math.random() * alfabet.length;
    const random = Math.floor(randomLetter);
    const letter = alfabet[random];
    return letter;
}

function generate(number) {

  let string = '';
  for (let i = 0; i < number; i++) {
      const randomLetter = pickRandomLetter();
      string += randomLetter;
  }
  return string
}


// ************* zadanie 4 **************

function modifyString(str) {
  let newString = '';
  for (let i = 0; i < str.length; i++) {
      const currentLetter = str.charAt(i);
 
      switch(currentLetter) {
          case 'a':
              newString += '4';
              break;
          case 'e':
              newString += '3';
              break;
          case 'i':
              newString += '1';
              break;
          case 'o':
              newString += '0';
              break;
          case 's':
              newString += '2';
              break;
          default:
              newString += currentLetter;
      }

  }
  return newString;
}